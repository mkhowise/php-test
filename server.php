<?php
// Establish a DB connection
$servername = "139.162.154.66:3306";
$db_name = "bitintel_db_interview";
$username = "bitintel_test_env";
$password = "!lovephp!";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$db_name", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
    die;
}

if (!empty($_POST)) {
    try {
        // Begin Transaction
        $conn->beginTransaction();

        // prepare sql and bind parameters for the clients
        $stmt = $conn->prepare("INSERT INTO `clients` (`name`, `email`) VALUES (:name, :email)");
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":email", $email);

        $name = $_POST["name"];
        $email = $_POST["email"];

        // Execute the query
        $stmt->execute();

        // Get the generated `client_id`
        $client_id = $conn->lastInsertId();

        // prepare sql and bind parameters for the companies
        $stmt = $conn->prepare("INSERT INTO `companies` (`company_name`) VALUES (:company_name)");
        $stmt->bindParam(":company_name", $company_name);

        $company_name = $_POST["company_name"];

        // Execute the query
        $stmt->execute();

        // Get the generated `company_id`
        $company_id = $conn->lastInsertId();

        // prepare sql and bind parameters for the companies_clients_r
        $stmt = $conn->prepare("INSERT INTO `companies_clients_r` (`client_id`, `company_id`) VALUES (:client_id, 
                                                                          :company_id)");
        $stmt->bindParam(":client_id", $client_id);
        $stmt->bindParam(":company_id", $company_id);

        // Execute the query
        $stmt->execute();

        // Make the changes to the database permanent
        $conn->commit();

        // Get last added client and company
        $query = $conn->prepare("SELECT `name`, `email`, `company_name` 
                                        FROM `companies_clients_r` 
                                        JOIN `clients` ON `companies_clients_r`.`client_id` = `clients`.`id`
                                        JOIN `companies` ON `companies_clients_r`.`company_id` = `companies`.`id`
                                        WHERE `clients`.`id` = $client_id");
        // Execute the query
        $query->execute();

        // Set the resulting array to associative
        $query->setFetchMode(PDO::FETCH_OBJ);

        // Print the result
        echo json_encode($query->fetch());

    } catch (PDOException $e) {
        // Failed to insert, rollback any changes
        $conn->rollback();
        echo "Error message: " . $e->getMessage();
    }
} elseif (isset($_GET['api']) && $_GET['api'] == 'match') {
    // Create & initialize a curl session
    $curl = curl_init();

    // Set our url with curl_setopt()
    curl_setopt($curl, CURLOPT_URL, "https://ah-devsec.com/test/");

    // Return the transfer as a string, also with setopt()
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    // curl_exec() executes the started curl session
    // $output contains the output string
    $output = curl_exec($curl);

    // Convert the output to assoc array
    $output = json_decode($output, true);

    // Close curl resource to free up system resources
    // (deletes the variable made by curl_init)
    curl_close($curl);

    // list of emails
    $listEmails = [];

    foreach ($output as $value) {
        array_push($listEmails, "'" .$value['email'] . "'");
    }

    // Convert array into string
    $emails = implode(", ", $listEmails);

    // Get matched clients and companies from the api
    $query = $conn->prepare("SELECT `name`, `email`, `company_name`
                                        FROM `companies_clients_r`
                                        JOIN `clients` ON `companies_clients_r`.`client_id` = `clients`.`id`
                                        JOIN `companies` ON `companies_clients_r`.`company_id` = `companies`.`id`
                                        WHERE `clients`.`email` IN ($emails)");

    // Execute the query
    $query->execute();

    // Set the resulting array to associative
    $query->setFetchMode(PDO::FETCH_ASSOC);

    // Print the result
    echo json_encode($query->fetchAll());
} else {
    // Get all existing clients and companies they benefit from
    $query = $conn->prepare("SELECT `name`, `email`, `company_name` 
                                        FROM `companies_clients_r` 
                                        JOIN `clients` ON `companies_clients_r`.`client_id` = `clients`.`id`
                                        JOIN `companies` ON `companies_clients_r`.`company_id` = `companies`.`id`");
    // Execute the query
    $query->execute();

    // Set the resulting array to associative
    $query->setFetchMode(PDO::FETCH_ASSOC);

    // Print the result
    echo json_encode($query->fetchAll());
}

// Close db connection
$conn = null;
?>