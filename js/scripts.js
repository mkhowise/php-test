$(function (){
    // Add new client
    $('#companyClientsForm').submit(function(e) {
        e.preventDefault();
        $.ajax({
            method: "POST",
            dataType: "json",
            url: "server.php",
            data: $(this).serialize()
        }).done(function( data ) {
            $("#clientsCompaniesTable").append("<tr><td>"+data.name+"</td><td>"+data.email+"</td><td>"+data.company_name+"</td></tr>")
        });
    });

    // Get all existing clients and companies
    $.ajax({
        dataType: "json",
        url: "server.php",
    }).done(function( data ) {
        $.each(data, function (key, value) {
            $("#clientsCompaniesTable").append("<tr><td>"+value.name+"</td><td>"+value.email+"</td><td>"+value.company_name+"</td></tr>")
        });
    });

    // Get matched clients and companies from the api
    $.ajax({
        dataType: "json",
        url: "server.php?api=match",
    }).done(function( data ) {
        $.each(data, function (key, value) {
            $("#matchedClientsTable").append("<tr><td>"+value.name+"</td><td>"+value.email+"</td><td>"+value.company_name+"</td></tr>")
        });
    });
});